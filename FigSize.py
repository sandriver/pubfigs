"""
options to be configured to each figure
"""
MaxSize = {
    'A4': (8.27, 11.69),
    'A3': (11.69, 16.53),
    'custom': None,
}

FigSize = {
    'A4': (8.27, 11.69),
    'A3': (11.69, 16.53),
    }

Units = ('cm', 'inch')

# figure size
# matplotlib seems only use inch
def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], (tuple, list)):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)


def fig_size(size, unit='cm', format='A4'):
    """
    :param size: string or (tuple, list). if string, it must be a key in FigSize
    :param unit: unit of the size
    :param format: string, paper format for the figure
    :return:
        tuple, figure size in inches
    """
    if isinstance(size, str):
        fsize = FigSize[size]
    elif isinstance(size, (tuple, list)):
        if unit == 'cm':
            fsize = cm2inch(size)
        elif unit == 'inch':
            fsize = size
        else:
            raise ValueError('Unit: {} not known')
    else:
        raise ValueError('input size must be a tuple, list or string')

    if MaxSize[format]:
        if fsize[0] > MaxSize[format][0] or fsize[1] > MaxSize[format][1]:
            raise ValueError('Specified figure size: {} is larger than allowed by the format: {}: {}'.
                             format(size, format, MaxSize[format]))
    return fsize
